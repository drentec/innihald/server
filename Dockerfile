FROM registry.gitlab.com/drentec/docker/dev-container

COPY . /app

EXPOSE 8080
ENV JAVA_OPTS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager"